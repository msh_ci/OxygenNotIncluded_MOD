﻿using System.Collections.Generic;
using HarmonyLib;
using TUNING;

namespace mymod
{
    public static class Patch_GourmetCookingStation// 建筑固定代码
    {
        [HarmonyPatch(typeof(GourmetCookingStationConfig))]// 燃气灶
        [HarmonyPatch("ConfigureRecipes")]
        public static class Patch_GourmetCookingStationConfig
        {
            [HarmonyPrefix]
            public static bool Prefix(GourmetCookingStationConfig __instance)
            {
                // 完全替换原有方法的逻辑
                CustomConfigureRecipes(__instance);
                return false; // 阻止原始方法执行
            }
            public static void CustomConfigureRecipes(GourmetCookingStationConfig __instance)
            {
                ComplexRecipe.RecipeElement[] array = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("GrilledPrickleFruit", 2f),
                    new ComplexRecipe.RecipeElement(SpiceNutConfig.ID, 2f)
                };
                ComplexRecipe.RecipeElement[] array2 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("Salsa", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                SalsaConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("GourmetCookingStation", array, array2), array, array2)
                {
                    time = FOOD.RECIPES.STANDARD_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.SALSA.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "GourmetCookingStation"
                    },
                    sortOrder = 300
                };
                ComplexRecipe.RecipeElement[] array3 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("FriedMushroom", 1f),
                    new ComplexRecipe.RecipeElement("Lettuce", 4f)
                };
                ComplexRecipe.RecipeElement[] array4 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("MushroomWrap", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                MushroomWrapConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("GourmetCookingStation", array3, array4), array3, array4)
                {
                    time = FOOD.RECIPES.STANDARD_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.MUSHROOMWRAP.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "GourmetCookingStation"
                    },
                    sortOrder = 400
                };
                ComplexRecipe.RecipeElement[] array5 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("CookedMeat", 1f),
                    new ComplexRecipe.RecipeElement("CookedFish", 1f)
                };
                ComplexRecipe.RecipeElement[] array6 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("SurfAndTurf", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                SurfAndTurfConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("GourmetCookingStation", array5, array6), array5, array6)
                {
                    time = FOOD.RECIPES.STANDARD_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.SURFANDTURF.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "GourmetCookingStation"
                    },
                    sortOrder = 500
                };
                ComplexRecipe.RecipeElement[] array7 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("ColdWheatSeed", 10f),
                    new ComplexRecipe.RecipeElement(SpiceNutConfig.ID, 1f)
                };
                ComplexRecipe.RecipeElement[] array8 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("SpiceBread", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                SpiceBreadConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("GourmetCookingStation", array7, array8), array7, array8)
                {
                    time = FOOD.RECIPES.STANDARD_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.SPICEBREAD.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "GourmetCookingStation"
                    },
                    sortOrder = 600
                };
                ComplexRecipe.RecipeElement[] array9 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("Tofu", 1f),
                    new ComplexRecipe.RecipeElement(SpiceNutConfig.ID, 1f)
                };
                ComplexRecipe.RecipeElement[] array10 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("SpicyTofu", 2f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                SpicyTofuConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("GourmetCookingStation", array9, array10), array9, array10)
                {
                    time = FOOD.RECIPES.STANDARD_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.SPICYTOFU.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "GourmetCookingStation"
                    },
                    sortOrder = 800
                };
                ComplexRecipe.RecipeElement[] array11 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(GingerConfig.ID, 4f),
                    new ComplexRecipe.RecipeElement("BeanPlantSeed", 4f)
                };
                ComplexRecipe.RecipeElement[] array12 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("Curry", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                SpicyTofuConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("GourmetCookingStation", array11, array12), array11, array12)
                {
                    time = FOOD.RECIPES.STANDARD_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.CURRY.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "GourmetCookingStation"
                    },
                    sortOrder = 800
                };
                ComplexRecipe.RecipeElement[] array13 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("CookedEgg", 1f),
                    new ComplexRecipe.RecipeElement("Lettuce", 1f),
                    new ComplexRecipe.RecipeElement("FriedMushroom", 1f)
                };
                ComplexRecipe.RecipeElement[] array14 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("Quiche", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                QuicheConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("GourmetCookingStation", array13, array14), array13, array14)
                {
                    time = FOOD.RECIPES.STANDARD_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.QUICHE.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "GourmetCookingStation"
                    },
                    sortOrder = 800
                };
                ComplexRecipe.RecipeElement[] array15 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("ColdWheatBread", 1f),
                    new ComplexRecipe.RecipeElement("Lettuce", 1f),
                    new ComplexRecipe.RecipeElement("CookedMeat", 1f)
                };
                ComplexRecipe.RecipeElement[] array16 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("Burger", 2f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                BurgerConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("GourmetCookingStation", array15, array16), array15, array16)
                {
                    time = FOOD.RECIPES.STANDARD_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.BURGER.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "GourmetCookingStation"
                    },
                    sortOrder = 900
                };
                if (DlcManager.IsExpansion1Active())
                {
                    ComplexRecipe.RecipeElement[] array17 = new ComplexRecipe.RecipeElement[]
                    {
                        new ComplexRecipe.RecipeElement("ColdWheatSeed", 3f),
                        new ComplexRecipe.RecipeElement("WormSuperFruit", 4f),
                        new ComplexRecipe.RecipeElement("GrilledPrickleFruit", 1f)
                    };
                    ComplexRecipe.RecipeElement[] array18 = new ComplexRecipe.RecipeElement[]
                    {
                        new ComplexRecipe.RecipeElement("BerryPie", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                    };
                    BerryPieConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("GourmetCookingStation", array17, array18), array17, array18)
                    {
                        time = FOOD.RECIPES.STANDARD_COOK_TIME,
                        description = STRINGS.ITEMS.FOOD.BERRYPIE.RECIPEDESC,
                        nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                        fabricators = new List<Tag>
                        {
                            "GourmetCookingStation"
                        },
                        sortOrder = 900
                    };
                }
            }
        }
    }
}
