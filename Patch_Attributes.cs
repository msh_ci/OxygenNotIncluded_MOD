﻿using System.Collections.Generic;
using System.Linq;
using Database;
using HarmonyLib;

namespace mymod
{
    [HarmonyPatch(typeof(Attributes), MethodType.Constructor, typeof(ResourceSet))]
    public class Patch_Attributes
    {
        public static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instr)
        {
            List<CodeInstruction> code = instr.ToList();
            code[90].operand = 10f;// 科学
            code[190].operand = 20f;// 农业
            code[556].operand = -0.5f;// 辐射
            return code.AsEnumerable();
        }
    }
}
