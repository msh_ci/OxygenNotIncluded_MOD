﻿using HarmonyLib;

namespace mymod
{
    public class Patch_Wires// 电线固定代码
    {
        [HarmonyPatch(typeof(Wire))]// 电线负载
        [HarmonyPatch("GetMaxWattageAsFloat")]
        public static class Patch_Wire
        {
            public static bool Prefix(ref float __result, Wire.WattageRating rating)
            {
                switch (rating)
                {
                    case Wire.WattageRating.Max500:
                        __result = 500f;
                        break;
                    case Wire.WattageRating.Max1000:
                        __result = 1000f;
                        break;
                    case Wire.WattageRating.Max2000:
                        __result = 4000f; // 修改为4000f
                        break;
                    case Wire.WattageRating.Max20000:
                        __result = 20000f;
                        break;
                    case Wire.WattageRating.Max50000:
                        __result = 80000f;// 修改为80000f
                        break;
                    default:
                        __result = 0f;
                        break;
                }
                return false; // 返回false以阻止原方法执行
            }
        }
    }
}
