﻿using System.Collections.Generic;
using HarmonyLib;

namespace mymod
{
    internal class Patch_GeyserGenericConfig
    {
        [HarmonyPatch(typeof(GeyserGenericConfig))]
        [HarmonyPatch("GenerateConfigs")]
        public class Patch_GenerateConfigs
        {
            public static void Postfix(ref List<GeyserGenericConfig.GeyserPrefabParams> __result)
            {
                // Debug.Log($"Number of geysers: {__result.Count}");
                for (int i = 0; i < __result.Count; i++)
                {
                    Debug.Log($"Number of geysers: {__result[i].geyserType.diseaseInfo.count}");
                }
                __result[2].geyserType.temperature = 318.15f;// 水45度
                __result[4].geyserType.diseaseInfo.count = 0;// 污水泉0病菌
                __result[6].geyserType.temperature = 303.15f;// 盐水30度
                __result[11].geyserType.temperature = 673.15f;// 氢气400度
                __result[11].geyserType.minRatePerCycle = 200f;// 提升产出
                __result[11].geyserType.maxRatePerCycle = 400f;
                __result[23].geyserType.temperature = 653.15f;// 原油380度
                __result[23].geyserType.minRatePerCycle = 200f;
                __result[23].geyserType.maxRatePerCycle = 400f;
            }
        }
    }
}