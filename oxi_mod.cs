﻿using HarmonyLib;
using KMod;
using TUNING;
using UnityEngine;
using BUILDINGS = TUNING.BUILDINGS;

namespace mymod
/*
1.食物不会修改
*/
{
    public class Mod : UserMod2// 官方接口，游戏运行时加载harmony补丁
    {
        public override void OnLoad(Harmony harmony)
        {
            base.OnLoad(harmony);
            Traverse.Create(typeof(DUPLICANTSTATS)).Field<int>("APTITUDE_BONUS").Value = 10;// 学习兴趣技能士气+10
            Traverse.Create(typeof(DUPLICANTSTATS)).Field<int[]>("APTITUDE_ATTRIBUTE_BONUSES").Value = new int[]
            {
                7,
                6,
                5
            };
            Traverse.Create(typeof(DUPLICANTSTATS.ATTRIBUTE_LEVELING)).Field<float>("FULL_EXPERIENCE").Value = 3f;// 经验获取倍率2倍
            Traverse.Create(typeof(ROLES)).Field<float>("BASIC_ROLE_MASTERY_EXPERIENCE_REQUIRED").Value = 200f;// 获取技能点的经验，越小越快
            Traverse.Create(typeof(SKILLS)).Field<float>("PASSIVE_EXPERIENCE_PORTION").Value = 1f;// 被动经验获倍率，越大越快
            Traverse.Create(typeof(EQUIPMENT.SUITS)).Field<float>("ATMOSUIT_DECAY").Value = -0.01f;// 气压服磨损
            Traverse.Create(typeof(EQUIPMENT.SUITS)).Field<int>("ATMOSUIT_INSULATION").Value = 100;// 气压服隔热+100
            Traverse.Create(typeof(EQUIPMENT.SUITS)).Field<int>("ATMOSUIT_ATHLETICS").Value = -2;// 气压服运动加成-2
            Traverse.Create(typeof(EQUIPMENT.SUITS)).Field<int>("ATMOSUIT_DIGGING").Value = 20;// 气压服挖掘加成+20
            Traverse.Create(typeof(EQUIPMENT.SUITS)).Field<int>("ATMOSUIT_CONSTRUCTION").Value = 20;// 气压服建造加成+20
            Traverse.Create(typeof(FOOD.RECIPES)).Field<float>("SMALL_COOK_TIME").Value = 20f;// 简单食物烹饪时间20s
            Traverse.Create(typeof(FOOD.RECIPES)).Field<float>("STANDARD_COOK_TIME").Value = 30f;// 复杂食物烹饪时间30s
            harmony.PatchAll();
        }
    }
    public class Patch_buildings// 建筑固定代码
    {
        [HarmonyPatch(typeof(GeneratorConfig))]// 煤炭发电机
        [HarmonyPatch("CreateBuildingDef")]
        public class Patch_Generator
        {
            public static void Postfix(ref BuildingDef __result)
            {
                __result.GeneratorWattageRating = 3000f;// 修改发电
            }
        }

        [HarmonyPatch(typeof(HydrogenGeneratorConfig))]// 氢气发电机
        [HarmonyPatch("CreateBuildingDef")]
        public class Patch_HydrogenGenerator
        {
            public static void Postfix(ref BuildingDef __result)
            {
                __result.GeneratorWattageRating = 4000f;// 修改发电
            }
        }

        [HarmonyPatch(typeof(BatterySmartConfig))]// 智能电池
        [HarmonyPatch("DoPostConfigureComplete")]
        public class Patch_BatterySmart
        {
            public static void Postfix(GameObject go)
            {
                BatterySmart batterySmart = go.AddOrGet<BatterySmart>();
                batterySmart.capacity = 40000f;// 修改储电量
                batterySmart.joulesLostPerSecond = 0f;
            }
        }

        [HarmonyPatch(typeof(BatteryMediumConfig))]// 巨型电池
        [HarmonyPatch("DoPostConfigureComplete")]
        public class Patch_BatteryMedium
        {
            public static void Postfix(GameObject go)
            {
                Battery battery = go.AddOrGet<Battery>();
                battery.capacity = 80000f;// 修改储电量
                battery.joulesLostPerSecond = 1f;
            }
        }

        [HarmonyPatch(typeof(CO2ScrubberConfig))]// 碳素脱离器
        [HarmonyPatch("ConfigureBuildingTemplate")]
        public class Patch_CO2Scrubber
        {
            public static void Postfix(GameObject go, Tag prefab_tag)
            {
                go.AddOrGet<ElementConverter>().consumedElements = new ElementConverter.ConsumedElement[]
                {
                    new ElementConverter.ConsumedElement(GameTagExtensions.Create(SimHashes.Water), 1f, true),
                    new ElementConverter.ConsumedElement(GameTagExtensions.Create(SimHashes.CarbonDioxide), 0.6f, true)// 吸收600g二氧化碳
                };
            }
        }

        [HarmonyPatch(typeof(ElectrolyzerConfig))]// 电解器
        [HarmonyPatch("ConfigureBuildingTemplate")]
        public class Patch_Electrolyzer
        {
            public static void Postfix(GameObject go, Tag prefab_tag)
            {
                CellOffset cellOffset = new CellOffset(0, 1);
                go.AddOrGet<ElementConverter>().outputElements = new ElementConverter.OutputElement[]
                {
                    new ElementConverter.OutputElement(8.8f, SimHashes.Oxygen, 343.15f, false, false, (float)cellOffset.x, (float)cellOffset.y, 1f, byte.MaxValue, 0, true),// 修改电解产出
                    new ElementConverter.OutputElement(1.2f, SimHashes.Hydrogen, 343.15f, false, false, (float)cellOffset.x, (float)cellOffset.y, 1f, byte.MaxValue, 0, true)
                };
            }
        }

        [HarmonyPatch(typeof(CompostConfig))]// 堆肥堆
        [HarmonyPatch("ConfigureBuildingTemplate")]
        public static class Patch_Compost
        {
            public static void Postfix(GameObject go, Tag prefab_tag)
            {
                go.AddOrGet<ElementConverter>().consumedElements = new ElementConverter.ConsumedElement[]
                {
                    new ElementConverter.ConsumedElement(CompostConfig.COMPOST_TAG, 0.2f, true)// 增加降解速率
                };
                go.AddOrGet<ElementConverter>().outputElements = new ElementConverter.OutputElement[]
                {
                    new ElementConverter.OutputElement(0.2f, SimHashes.Dirt, 348.15f, false, true, 0f, 0.5f, 1f, byte.MaxValue, 0, true)// 增加产物速率
                };
            }
        }

        [HarmonyPatch(typeof(LiquidHeaterConfig))]// 液体加热器
        [HarmonyPatch("CreateBuildingDef")]
        public class Patch_LiquidHeater_CreateBuildingDef
        {
            public static void Postfix(ref BuildingDef __result)
            {
                __result.EnergyConsumptionWhenActive = 900f;// 修改耗电
                __result.ExhaustKilowattsWhenActive = 6000f;// 修改发热
                __result.OverheatTemperature = 693.15f;// 修改过热温度
            }
        }

        [HarmonyPatch(typeof(LiquidHeaterConfig))]// 液体加热器建筑属性
        [HarmonyPatch("ConfigureBuildingTemplate")]
        public class Patch_LiquidHeater_ConfigureBuildingTemplate
        {
            public static void Postfix(GameObject go, Tag prefab_tag)
            {
                go.AddOrGet<LoopingSounds>();
                SpaceHeater spaceHeater = go.AddOrGet<SpaceHeater>();
                spaceHeater.SetLiquidHeater();
                spaceHeater.targetTemperature = 693.15f;// 420摄氏度，可直接炼油
                spaceHeater.minimumCellMass = 10f;
            }
        }

        [HarmonyPatch(typeof(InsulationTileConfig))]// 隔热砖
        [HarmonyPatch("CreateBuildingDef")]
        public class Patch_InsulationTile
        {
            public static void Postfix(ref BuildingDef __result)
            {
                __result.BaseDecor = (float)BUILDINGS.DECOR.BONUS.TIER2.amount;// 隔热砖装饰加成
                __result.BaseDecorRadius = (float)BUILDINGS.DECOR.BONUS.TIER2.radius;// 隔热砖装饰加成范围
            }
        }

        [HarmonyPatch(typeof(WoodTileConfig))]// 木砖
        [HarmonyPatch("ConfigureBuildingTemplate")]
        public class Patch_WoodTile
        {
            public static void Postfix(GameObject go, Tag prefab_tag)
            {
                go.AddOrGet<SimCellOccupier>().movementSpeedMultiplier = DUPLICANTSTATS.MOVEMENT_MODIFIERS.BONUS_3;// 木砖加1.5倍移速
            }
        }

        [HarmonyPatch(typeof(CarpetTileConfig))]// 地毯砖
        [HarmonyPatch("ConfigureBuildingTemplate")]
        public class Patch_CarpetTile
        {
            public static void Postfix(GameObject go, Tag prefab_tag)
            {
                go.AddOrGet<SimCellOccupier>().movementSpeedMultiplier = DUPLICANTSTATS.MOVEMENT_MODIFIERS.BONUS_1;// 地毯砖加1.1倍移速
            }
        }

        [HarmonyPatch(typeof(PolymerizerConfig))]// 塑料制造机
        [HarmonyPatch("ConfigureBuildingTemplate")]
        public class Patch_Polymerizer
        {
            public static void Postfix(GameObject go, Tag prefab_tag)
            {
                go.AddOrGet<Polymerizer>().exhaustElement = SimHashes.Hydrogen;
                go.AddOrGet<ElementConverter>().outputElements = new ElementConverter.OutputElement[]
                {
                    new ElementConverter.OutputElement(5f, SimHashes.Polypropylene, 348.15f, false, true, 0f, 0.5f, 1f, byte.MaxValue, 0, true),// 增加塑料产量
                    new ElementConverter.OutputElement(0.05f, SimHashes.Hydrogen, 473.15f, false, true, 0f, 0.5f, 1f, byte.MaxValue, 0, true),
                    new ElementConverter.OutputElement(0.05f, SimHashes.Hydrogen, 423.15f, false, true, 0f, 0.5f, 1f, byte.MaxValue, 0, true)// 产出氢气50g
                };
            }
        }

        [HarmonyPatch(typeof(FertilizerMakerConfig))]// 肥料合成器
        [HarmonyPatch("ConfigureBuildingTemplate")]
        public class Patch_FertilizerMaker
        {
            public static void Postfix(GameObject go, Tag prefab_tag)
            {
                go.AddOrGet<ElementConverter>().consumedElements = new ElementConverter.ConsumedElement[]
                {
                    new ElementConverter.ConsumedElement(new Tag("DirtyWater"), 0.01f, true),
                    new ElementConverter.ConsumedElement(new Tag("Dirt"), 0.01f, true),
                    new ElementConverter.ConsumedElement(new Tag("Phosphorite"), 0.001f, true)
                };
                go.AddOrGet<ElementConverter>().outputElements = new ElementConverter.OutputElement[]
                {
                    new ElementConverter.OutputElement(1.2f, SimHashes.Fertilizer, 323.15f, false, true, 0f, 0.5f, 1f, byte.MaxValue, 0, true)
                };
                BuildingElementEmitter buildingElementEmitter = go.AddOrGet<BuildingElementEmitter>();
                buildingElementEmitter.emitRate = 0.1f;
                buildingElementEmitter.temperature = 298.15f;
                buildingElementEmitter.element = SimHashes.Hydrogen;
            }
        }

        [HarmonyPatch(typeof(FarmStationConfig))]// 肥料合成器
        [HarmonyPatch("DoPostConfigureComplete")]
        public class Patch_FarmStation
        {
            public static void Postfix(GameObject go)
            {
                go.AddOrGet<ManualDeliveryKG>().refillMass = 2f;
                go.AddOrGet<TinkerStation>().massPerTinker = 2f;
                go.GetComponent<KPrefabID>().prefabInitFn += delegate (GameObject game_object)
                {
                    TinkerStation component = game_object.GetComponent<TinkerStation>();
                    component.AttributeConverter = Db.Get().AttributeConverters.HarvestSpeed;
                    component.AttributeExperienceMultiplier = DUPLICANTSTATS.ATTRIBUTE_LEVELING.MOST_DAY_EXPERIENCE;
                    component.SkillExperienceSkillGroup = Db.Get().SkillGroups.Farming.Id;
                    component.SkillExperienceMultiplier = SKILLS.MOST_DAY_EXPERIENCE;
                    component.toolProductionTime = 5f;
                };
            }
        }

        [HarmonyPatch(typeof(MilkFatSeparatorConfig))]// 咸乳蜡收集器
        [HarmonyPatch("ConfigureBuildingTemplate")]
        public class Patch_MilkFatSeparator
        {
            public static void Postfix(GameObject go, Tag prefab_tag)
            {
                go.AddOrGet<ElementConverter>().outputElements = new ElementConverter.OutputElement[]
                {
                    new ElementConverter.OutputElement(0.8f, SimHashes.MilkFat, 0f, false, true, 0f, 0.5f, 1f, byte.MaxValue, 0, true),
                    new ElementConverter.OutputElement(0.1f, SimHashes.Brine, 0f, false, true, 0f, 0.5f, 0f, byte.MaxValue, 0, true),
                    new ElementConverter.OutputElement(0.1f, SimHashes.CarbonDioxide, 348.15f, false, false, 1f, 3f, 0f, byte.MaxValue, 0, true)
                };
            }
        }

        [HarmonyPatch(typeof(HotTubConfig))]// 热水浴缸
        [HarmonyPatch("ConfigureBuildingTemplate")]
        public class Patch_HotTub
        {
            public static void Postfix(GameObject go, Tag prefab_tag)
            {
                go.AddOrGet<HotTub>().bleachStoneConsumption = 0.01f;
            }
        }

        [HarmonyPatch(typeof(LogicDuplicantSensorConfig))]// 复制人运动传感器
        [HarmonyPatch("DoPostConfigureComplete")]
        public class Patch_LogicDuplicantSensor
        {
            public static void Postfix(GameObject go)
            {
                go.AddOrGet<LogicDuplicantSensor>().pickupRange = 6;
            }
        }

        [HarmonyPatch(typeof(LogicDuplicantSensorConfig))]// 复制人运动传感器显示范围
        [HarmonyPatch("AddVisualizer")]
        public class Patch_LogicDuplicantSensorAddVisualizer
        {
            public static void Postfix(GameObject prefab, bool movable)
            {
                RangeVisualizer rangeVisualizer = prefab.AddOrGet<RangeVisualizer>();
                rangeVisualizer.RangeMin.x = -3;
                rangeVisualizer.RangeMin.y = 0;
                rangeVisualizer.RangeMax.x = 3;
                rangeVisualizer.RangeMax.y = 6;
            }
        }

        [HarmonyPatch(typeof(LiquidPumpConfig))]// 液泵
        [HarmonyPatch("CreateBuildingDef")]
        public class Patch_LiquidPump
        {
            public static void Postfix(ref BuildingDef __result)
            {
                __result.EnergyConsumptionWhenActive = 200f;// 液泵耗电
            }
        }

        [HarmonyPatch(typeof(GasPumpConfig))]// 气泵
        [HarmonyPatch("CreateBuildingDef")]
        public class Patch_GasPump
        {
            public static void Postfix(ref BuildingDef __result)
            {
                __result.EnergyConsumptionWhenActive = 160f;// 气泵耗电
            }
        }

        [HarmonyPatch(typeof(DataMinerConfig))]// 数据挖掘仪
        [HarmonyPatch("CreateBuildingDef")]
        public class Patch_DataMiner
        {
            public static void Postfix(ref BuildingDef __result)
            {
                __result.SelfHeatKilowattsWhenActive = 1f;// 数据挖掘仪产热
            }
        }

        [HarmonyPatch(typeof(DataMinerConfig))]// 数据挖掘仪产出
        [HarmonyPatch("ConfigureBuildingTemplate")]
        public class Patch_DataMinerTemplate
        {
            public static void Postfix(GameObject go, Tag prefab_tag)
            {
                DataMiner dataMiner = go.AddOrGet<DataMiner>();
                BuildingTemplates.CreateComplexFabricatorStorage(go, dataMiner);
                ComplexRecipe.RecipeElement[] array = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(SimHashes.Polypropylene.CreateTag(), 1f)
                };
                ComplexRecipe.RecipeElement[] array2 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(DatabankHelper.TAG, 2f, ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false)
                };
            }
        }

        [HarmonyPatch(typeof(RemoteWorkTerminalConfig))]// 数据挖掘仪产出
        [HarmonyPatch("DoPostConfigureComplete")]
        public class Patch_RemoteWorkTerminal
        {
            public static void Postfix(GameObject go)
            {
                ElementConverter elementConverter = go.AddOrGet<ElementConverter>();
                elementConverter.consumedElements = new ElementConverter.ConsumedElement[]
                {
                    new ElementConverter.ConsumedElement(RemoteWorkTerminalConfig.INPUT_MATERIAL, 0.005f, true)
                };
            }
        }

        [HarmonyPatch(typeof(PowerTransformerConfig))]// 大型变压器
        [HarmonyPatch("CreateBuildingDef")]
        public class Patch_PowerTransformer
        {
            public static void Postfix(ref BuildingDef __result)
            {
                __result.SelfHeatKilowattsWhenActive = 0.5f;// 变压器发热
            }
        }

        [HarmonyPatch(typeof(TravelTubeEntranceConfig))]// 运载管道入口
        [HarmonyPatch("ConfigureBuildingTemplate")]
        public class Patch_TravelTubeEntrance
        {
            public static void Postfix(GameObject go, Tag prefab_tag)
            {
                TravelTubeEntrance travelTubeEntrance = go.AddOrGet<TravelTubeEntrance>();
                travelTubeEntrance.waxPerLaunch = 0.01f;
                travelTubeEntrance.joulesPerLaunch = 5000f;
                travelTubeEntrance.jouleCapacity = 60000f;
                ManualDeliveryKG manualDeliveryKG = go.AddOrGet<ManualDeliveryKG>();
                manualDeliveryKG.refillMass = 0.01f;
            }
        }

        [HarmonyPatch(typeof(SolidConduitOutboxConfig))]// 运输存放器
        [HarmonyPatch("ConfigureBuildingTemplate")]
        public class Patch_SolidConduitOutbox
        {
            public static void Postfix(GameObject go, Tag prefab_tag)
            {
                Storage storage = BuildingTemplates.CreateDefaultStorage(go, false);
                storage.capacityKg = 200f;
            }
        }

    }

    public class Patch_plants// 植物固定代码
    {
        [HarmonyPatch(typeof(PrickleFlowerConfig))]// 毛刺花
        [HarmonyPatch("CreatePrefab")]
        public static class Patch_PrickleFlower
        {
            public static void Postfix(GameObject __result)
            {
                if (__result != null)
                {
                    EntityTemplates.ExtendEntityToBasicPlant(__result, 208.15f, 258.15f, 348.15f, 398.15f, new SimHashes[]// 修改温度范围-15~75
                    {
                    SimHashes.Oxygen,
                    SimHashes.ContaminatedOxygen,
                    SimHashes.CarbonDioxide
                    }, true, 0f, 0.15f, PrickleFruitConfig.ID, true, true, true, true, 600f, 0f, 4600f, "PrickleFlowerOriginal", STRINGS.CREATURES.SPECIES.PRICKLEFLOWER.NAME);

                    EntityTemplates.ExtendPlantToIrrigated(__result, new PlantElementAbsorber.ConsumeInfo[]
                    {
                    new PlantElementAbsorber.ConsumeInfo
                    {
                        tag = GameTags.Water,
                        massConsumptionRate = 0.01f// 修改耗水6kg/周期
                    }
                    });
                }
            }
        }

        [HarmonyPatch(typeof(ColdWheatConfig))]// 冰霜小麦
        [HarmonyPatch("CreatePrefab")]
        public static class Patch_ColdWheat
        {
            public static void Postfix(GameObject __result)
            {
                if (__result != null)
                {
                    EntityTemplates.ExtendEntityToBasicPlant(__result, 153.15f, 203.15f, 303.15f, 353.15f, new SimHashes[]// 修改温度范围-70~30
                    {
                        SimHashes.Oxygen,
                        SimHashes.ContaminatedOxygen,
                        SimHashes.CarbonDioxide
                    }, true, 0f, 0.15f, "ColdWheatSeed", true, true, true, true, 600f, 0f, 12200f, "ColdWheatOriginal", STRINGS.CREATURES.SPECIES.COLDWHEAT.NAME);
                    EntityTemplates.ExtendPlantToFertilizable(__result, new PlantElementAbsorber.ConsumeInfo[]
                    {
                        new PlantElementAbsorber.ConsumeInfo
                        {
                            tag = GameTags.Dirt,
                            massConsumptionRate = 0.001f// 修改消耗泥土0.6kg/周期
                        }
                    });
                    EntityTemplates.ExtendPlantToIrrigated(__result, new PlantElementAbsorber.ConsumeInfo[]
                    {
                        new PlantElementAbsorber.ConsumeInfo
                        {
                            tag = GameTags.Water,
                            massConsumptionRate = 0.01f// 修改消耗水6kg/周期
                        }
                    });
                }
            }
        }

        [HarmonyPatch(typeof(MushroomPlantConfig))]// 夜幕菇
        [HarmonyPatch("CreatePrefab")]
        public static class Patch_MushroomPlant
        {
            public static void Postfix(GameObject __result)
            {
                if (__result != null)
                {
                    EntityTemplates.ExtendEntityToBasicPlant(__result, 208.15f, 258.15f, 348.15f, 398.15f, new SimHashes[]// 修改温度范围-15~75
		            {
			            SimHashes.CarbonDioxide
		            }, true, 0f, 0.15f, MushroomConfig.ID, true, true, true, true, 600f, 0f, 4600f, "MushroomPlantOriginal", STRINGS.CREATURES.SPECIES.MUSHROOMPLANT.NAME);
		            EntityTemplates.ExtendPlantToFertilizable(__result, new PlantElementAbsorber.ConsumeInfo[]
		            {
			            new PlantElementAbsorber.ConsumeInfo
			            {
				            tag = GameTags.Dirt,
				            massConsumptionRate = 0.001f// 修改消耗泥土0.6kg/周期
			            }
                    });
                }
            }
        }

        [HarmonyPatch(typeof(BeanPlantConfig))]// 小吃芽
        [HarmonyPatch("CreatePrefab")]
        public static class Patch_BeanPlant
        {
            public static void Postfix(GameObject __result)
            {
                if (__result != null)
                {
                    GameObject template = __result;
                    float temperature_lethal_low = 153.15f;// 修改温度范围-70~30
                    float temperature_warning_low = 203.15f;
                    float temperature_warning_high = 303.15f;
                    float temperature_lethal_high = 353.15f;
                    string text = STRINGS.CREATURES.SPECIES.BEAN_PLANT.NAME;
                    EntityTemplates.ExtendEntityToBasicPlant(template, temperature_lethal_low, temperature_warning_low, temperature_warning_high, temperature_lethal_high, new SimHashes[]
                    {
                        SimHashes.CarbonDioxide
                    }, true, 0f, 0.025f, "BeanPlantSeed", true, true, true, true, 600f, 0f, 9800f, "BeanPlantOriginal", text);
                    EntityTemplates.ExtendPlantToIrrigated(__result, new PlantElementAbsorber.ConsumeInfo[]
                    {
                        new PlantElementAbsorber.ConsumeInfo
                        {
                            tag = SimHashes.Water.CreateTag(),
                            massConsumptionRate = 0.01f// 修改消耗水6kg/周期
                        }
                    });
                    EntityTemplates.ExtendPlantToFertilizable(__result, new PlantElementAbsorber.ConsumeInfo[]
                    {
                        new PlantElementAbsorber.ConsumeInfo
                        {
                            tag = SimHashes.Dirt.CreateTag(),
                            massConsumptionRate = 0.001f// 修改消耗泥土0.6kg/周期
                        }
                    });
                }
            }
        }

        [HarmonyPatch(typeof(SeaLettuceConfig))]// 水草
        [HarmonyPatch("CreatePrefab")]
        public static class Patch_SeaLettuce
        {
            public static void Postfix(GameObject __result)
            {
                if (__result != null)
                {
                    EntityTemplates.ExtendEntityToBasicPlant(__result, 193.15f, 243.15f, 343.15f, 393.15f, new SimHashes[]// 修改温度范围-30~70
                    {
                        SimHashes.Water,
                        SimHashes.SaltWater,
                        SimHashes.Brine
                    }, false, 0f, 0.15f, "Lettuce", true, true, true, true, 600f, 0f, 7400f, SeaLettuceConfig.ID + "Original", STRINGS.CREATURES.SPECIES.SEALETTUCE.NAME);
                    EntityTemplates.ExtendPlantToIrrigated(__result, new PlantElementAbsorber.ConsumeInfo[]
                    {
                        new PlantElementAbsorber.ConsumeInfo
                        {
                            tag = SimHashes.SaltWater.CreateTag(),
                            massConsumptionRate = 0.01f// 修改消耗盐水6kg/周期
                        }
                    });
                    EntityTemplates.ExtendPlantToFertilizable(__result, new PlantElementAbsorber.ConsumeInfo[]
                    {
                        new PlantElementAbsorber.ConsumeInfo
                        {
                            tag = SimHashes.IgneousRock.CreateTag(),
                            massConsumptionRate = 0.001f// 修改消耗火成岩0.6kg/周期
                        }
                    });
                }
            }
        }

        [HarmonyPatch(typeof(SpiceVineConfig))]// 火椒藤
        [HarmonyPatch("CreatePrefab")]
        public static class Patch_SpiceVine
        {
            public static void Postfix(GameObject __result)
            {
                if (__result != null)
                {
                    EntityTemplates.ExtendEntityToBasicPlant(__result, 193.15f, 243.15f, 343.15f, 393.15f, null, true, 0f, 0.15f, SpiceNutConfig.ID, true, true, true, true, 600f, 0f, 9800f, "SpiceVineOriginal", STRINGS.CREATURES.SPECIES.SPICE_VINE.NAME);// 修改温度范围-30~70
                    Tag tag = ElementLoader.FindElementByHash(SimHashes.DirtyWater).tag;
                    EntityTemplates.ExtendPlantToIrrigated(__result, new PlantElementAbsorber.ConsumeInfo[]
                    {
                        new PlantElementAbsorber.ConsumeInfo
                        {
                            tag = tag,
                            massConsumptionRate = 0.01f// 修改消耗污染水6kg/周期
                        }
                    });
                    EntityTemplates.ExtendPlantToFertilizable(__result, new PlantElementAbsorber.ConsumeInfo[]
                    {
                        new PlantElementAbsorber.ConsumeInfo
                        {
                            tag = SimHashes.IgneousRock.CreateTag(),
                            massConsumptionRate = 0.001f// 修改消耗火成岩0.6kg/周期
                        }
                    });
                }
            }
        }

        [HarmonyPatch(typeof(SwampLilyConfig))]// 芳香百合
        [HarmonyPatch("CreatePrefab")]
        public static class Patch_SwampLily
        {
            public static void Postfix(GameObject __result)
            {
                if (__result != null)
                {
                    EntityTemplates.ExtendEntityToBasicPlant(__result, 238.15f, 288.15f, 368.15f, 418.15f, new SimHashes[]// 修改温度范围15~95
                    {
                        SimHashes.ChlorineGas
                    }, true, 0f, 0.15f, SwampLilyFlowerConfig.ID, true, true, true, true, 600f, 0f, 4600f, SwampLilyConfig.ID + "Original", STRINGS.CREATURES.SPECIES.SWAMPLILY.NAME);
                }
            }
        }

        [HarmonyPatch(typeof(BulbPlantConfig))]// 同伴芽
        [HarmonyPatch("CreatePrefab")]
        public static class Patch_BulbPlant
        {
            public static void Postfix(GameObject __result)
            {
                if (__result != null)
                {
                    EntityTemplates.ExtendEntityToBasicPlant(__result, 228f, 278.15f, 338.15f, 388.15f, new SimHashes[]// 修改温度范围5~65
                    {
                        SimHashes.Oxygen,
                        SimHashes.ContaminatedOxygen,
                        SimHashes.CarbonDioxide
                    }, true, 0f, 0.15f, null, true, false, true, true, 2400f, 0f, 2200f, "BulbPlantOriginal", STRINGS.CREATURES.SPECIES.BULBPLANT.NAME);
                }
            }
        }

        [HarmonyPatch(typeof(GasGrassConfig))]// 释气草
        [HarmonyPatch("CreatePrefab")]
        public static class Patch_GasGrass
        {
            public static void Postfix(GameObject __result)
            {
                if (__result != null)
                {
                    EntityTemplates.ExtendPlantToIrrigated(__result, new PlantElementAbsorber.ConsumeInfo[]
                    {
                        new PlantElementAbsorber.ConsumeInfo
                        {
                            tag = GameTags.Chlorine,
                            massConsumptionRate = 0.001f// 修改消耗液氯0.6kg/周期
                        }
                    });
                    EntityTemplates.ExtendPlantToFertilizable(__result, new PlantElementAbsorber.ConsumeInfo[]
                    {
                        new PlantElementAbsorber.ConsumeInfo
                        {
                            tag = SimHashes.Dirt.CreateTag(),
                            massConsumptionRate = 0.01f// 修改消耗泥土6kg/周期
                        }
                    });
                }
            }
        }

        [HarmonyPatch(typeof(ColdBreatherConfig))]// 冰息萝卜
        [HarmonyPatch("CreatePrefab")]
        public static class Patch_ColdBreather
        {
            public static void Postfix(GameObject __result)
            {
                if (__result != null)
                {
                    EntityTemplates.ExtendPlantToFertilizable(__result, new PlantElementAbsorber.ConsumeInfo[]
                    {
                        new PlantElementAbsorber.ConsumeInfo
                        {
                            tag = SimHashes.Phosphorite.CreateTag(),
                            massConsumptionRate = 0.001f// 修改消耗磷矿0.6kg/周期
                        }
                    });
                }
            }
        }

        [HarmonyPatch(typeof(BasicFabricMaterialPlantConfig))]// 顶针芦苇
        [HarmonyPatch("CreatePrefab")]
        public static class Patch_BasicFabricMaterialPlant
        {
            public static void Postfix(GameObject __result)
            {
                if (__result != null)
                {
                    GameObject template = __result;
                    float temperature_lethal_low = 238.15f;// 修改温度范围15~45
                    float temperature_warning_low = 288.15f;
                    float temperature_warning_high = 318.15f;
                    float temperature_lethal_high = 368.15f;
                    string text = BasicFabricConfig.ID;
                    EntityTemplates.ExtendEntityToBasicPlant(template, temperature_lethal_low, temperature_warning_low, temperature_warning_high, temperature_lethal_high, new SimHashes[]
                    {
                        SimHashes.Oxygen,
                        SimHashes.ContaminatedOxygen,
                        SimHashes.CarbonDioxide,
                        SimHashes.DirtyWater,
                        SimHashes.Water
                    }, false, 0f, 0.15f, text, false, true, true, true, 600f, 0f, 4600f, BasicFabricMaterialPlantConfig.ID + "Original", STRINGS.CREATURES.SPECIES.BASICFABRICMATERIALPLANT.NAME);
                    EntityTemplates.ExtendPlantToIrrigated(__result, new PlantElementAbsorber.ConsumeInfo[]
                    {
                        new PlantElementAbsorber.ConsumeInfo
                        {
                            tag = GameTags.DirtyWater,
                            massConsumptionRate = 0.05f// 修改消耗污染水30kg/周期
                        }
                    });
                }
            }
        }

        [HarmonyPatch(typeof(ForestTreeConfig))]// 乔木树
        [HarmonyPatch("CreatePrefab")]
        public static class Patch_ForestTree
        {
            public static void Postfix(GameObject __result)
            {
                if (__result != null)
                {
                    EntityTemplates.ExtendEntityToBasicPlant(__result, 233.15f, 283.15f, 368.15f, 418.15f, null, true, 0f, 0.15f, "WoodLog", true, true, true, false, 600f, 0f, 9800f, "ForestTreeOriginal", STRINGS.CREATURES.SPECIES.WOOD_TREE.NAME);// 修改温度范围10~95
                    Tag tag = ElementLoader.FindElementByHash(SimHashes.DirtyWater).tag;
                    EntityTemplates.ExtendPlantToIrrigated(__result, new PlantElementAbsorber.ConsumeInfo[]
                    {
                        new PlantElementAbsorber.ConsumeInfo
                        {
                            tag = tag,
                            massConsumptionRate = 0.01f// 修改消耗污染水6kg/周期
                        }
                    });
                    EntityTemplates.ExtendPlantToFertilizable(__result, new PlantElementAbsorber.ConsumeInfo[]
                    {
                        new PlantElementAbsorber.ConsumeInfo
                        {
                            tag = GameTags.Dirt,
                            massConsumptionRate = 0.01f// 修改消耗泥土6kg/周期
                        }
                    });
                }
            }
        }

        [HarmonyPatch(typeof(ForestTreeBranchConfig))]// 乔木树树枝
        [HarmonyPatch("CreatePrefab")]
        public static class Patch_ForestTreeBranch
        {
            public static void Postfix(GameObject __result)
            {
                if (__result != null)
                {
                    EntityTemplates.ExtendEntityToBasicPlant(__result, 258.15f, 288.15f, 313.15f, 448.15f, null, true, 0f, 0.15f, "WoodLog", true, true, false, true, 3000f, 0f, 9800f, "ForestTreeBranchOriginal", STRINGS.CREATURES.SPECIES.WOOD_TREE.NAME);// 修改温度范围15~95
                }
            }
        }

        [HarmonyPatch(typeof(IceFlowerConfig))]// 恬静花
        [HarmonyPatch("CreatePrefab")]
        public static class Patch_IceFlower
        {
            public static void Postfix(GameObject __result)
            {
                if (__result != null)
                {
                    EntityTemplates.ExtendEntityToBasicPlant(__result, 208f, 258.15f, 338.15f, 388.15f, new SimHashes[]// 修改温度范围-15~65
                    {
                        SimHashes.Oxygen,
                        SimHashes.ContaminatedOxygen,
                        SimHashes.CarbonDioxide,
                        SimHashes.ChlorineGas,
                        SimHashes.Hydrogen
                    }, true, 0f, 0.15f, null, true, false, true, true, 2400f, 0f, 2200f, "IceFlowerOriginal", STRINGS.CREATURES.SPECIES.ICEFLOWER.NAME);
                }
            }
        }
    }
}