﻿using System.Collections.Generic;
using HarmonyLib;
using STRINGS;
using UnityEngine;

namespace mymod
{
    public static class Patch_RockCrusher// 建筑固定代码
    {
        [HarmonyPatch(typeof(RockCrusherConfig))]// 碎石机
        [HarmonyPatch("ConfigureBuildingTemplate")]
        public static class Patch_RockCrusherConfig
        {
            [HarmonyPrefix]
            public static bool Prefix(ref object __instance, GameObject go, Tag prefab_tag)
            {
                go.GetComponent<KPrefabID>().AddTag(RoomConstraints.ConstraintTags.IndustrialMachinery, false);
                go.AddOrGet<DropAllWorkable>();
                go.AddOrGet<BuildingComplete>().isManuallyOperated = true;
                ComplexFabricator complexFabricator = go.AddOrGet<ComplexFabricator>();
                complexFabricator.sideScreenStyle = ComplexFabricatorSideScreen.StyleSetting.ListQueueHybrid;
                complexFabricator.duplicantOperated = true;
                go.AddOrGet<FabricatorIngredientStatusManager>();
                go.AddOrGet<CopyBuildingSettings>();
                ComplexFabricatorWorkable complexFabricatorWorkable = go.AddOrGet<ComplexFabricatorWorkable>();
                BuildingTemplates.CreateComplexFabricatorStorage(go, complexFabricator);
                complexFabricatorWorkable.overrideAnims = new KAnimFile[]
                {
                    Assets.GetAnim("anim_interacts_rockrefinery_kanim")
                };
                complexFabricatorWorkable.workingPstComplete = new HashedString[]
                {
                    "working_pst_complete"
                };
                Tag tag = SimHashes.Sand.CreateTag();
                foreach (Element element in ElementLoader.elements.FindAll((Element e) => e.HasTag(GameTags.Crushable)))
                {
                    ComplexRecipe.RecipeElement[] array = new ComplexRecipe.RecipeElement[]
                    {
                        new ComplexRecipe.RecipeElement(element.tag, 50f)// 修改岩矿石消耗
                    };
                    ComplexRecipe.RecipeElement[] array2 = new ComplexRecipe.RecipeElement[]
                    {
                        new ComplexRecipe.RecipeElement(SimHashes.Sand.CreateTag(), 100f)// 产出沙子100kg
                    };
                    string obsolete_id = ComplexRecipeManager.MakeObsoleteRecipeID("RockCrusher", element.tag);
                    string text = ComplexRecipeManager.MakeRecipeID("RockCrusher", array, array2);
                    ComplexRecipe complexRecipe = new ComplexRecipe(text, array, array2);
                    complexRecipe.time = 30f;// 修改时间
                    complexRecipe.description = string.Format(STRINGS.BUILDINGS.PREFABS.ROCKCRUSHER.RECIPE_DESCRIPTION, element.name, tag.ProperName());
                    complexRecipe.nameDisplay = ComplexRecipe.RecipeNameDisplay.IngredientToResult;
                    complexRecipe.fabricators = new List<Tag>
                    {
                        TagManager.Create("RockCrusher")
                    };
                    ComplexRecipeManager.Get().AddObsoleteIDMapping(obsolete_id, text);
                }
                foreach (Element element2 in ElementLoader.elements.FindAll((Element e) => e.IsSolid && e.HasTag(GameTags.Metal)))
                {
                    if (!element2.HasTag(GameTags.Noncrushable))
                    {
                        Element lowTempTransition = element2.highTempTransition.lowTempTransition;
                        if (lowTempTransition != element2)
                        {
                            ComplexRecipe.RecipeElement[] array3 = new ComplexRecipe.RecipeElement[]
                            {
                                new ComplexRecipe.RecipeElement(element2.tag, 50f)// 修改金属矿石消耗
                            };
                            ComplexRecipe.RecipeElement[] array4 = new ComplexRecipe.RecipeElement[]
                            {
                                new ComplexRecipe.RecipeElement(lowTempTransition.tag, 100f),// 产出精炼金属100kg
                            };
                            string obsolete_id2 = ComplexRecipeManager.MakeObsoleteRecipeID("RockCrusher", lowTempTransition.tag);
                            string text2 = ComplexRecipeManager.MakeRecipeID("RockCrusher", array3, array4);
                            ComplexRecipe complexRecipe2 = new ComplexRecipe(text2, array3, array4);
                            complexRecipe2.time = 30f;// 修改时间
                            complexRecipe2.description = string.Format(STRINGS.BUILDINGS.PREFABS.ROCKCRUSHER.METAL_RECIPE_DESCRIPTION, lowTempTransition.name, element2.name);
                            complexRecipe2.nameDisplay = ComplexRecipe.RecipeNameDisplay.IngredientToResult;
                            complexRecipe2.fabricators = new List<Tag>
                            {
                                TagManager.Create("RockCrusher")
                            };
                            ComplexRecipeManager.Get().AddObsoleteIDMapping(obsolete_id2, text2);
                        }
                    }
                }
                Element element3 = ElementLoader.FindElementByHash(SimHashes.Lime);
                ComplexRecipe.RecipeElement[] array5 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("EggShell", 5f)// 蛋壳产石灰
                };
                ComplexRecipe.RecipeElement[] array6 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(ElementLoader.FindElementByHash(SimHashes.Lime).tag, 5f, ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false)
                };
                string obsolete_id3 = ComplexRecipeManager.MakeObsoleteRecipeID("RockCrusher", element3.tag);
                string text3 = ComplexRecipeManager.MakeRecipeID("RockCrusher", array5, array6);
                ComplexRecipe complexRecipe3 = new ComplexRecipe(text3, array5, array6);
                complexRecipe3.time = 30f;// 修改时间
                complexRecipe3.description = string.Format(STRINGS.BUILDINGS.PREFABS.ROCKCRUSHER.LIME_RECIPE_DESCRIPTION, SimHashes.Lime.CreateTag().ProperName(), MISC.TAGS.EGGSHELL);
                complexRecipe3.nameDisplay = ComplexRecipe.RecipeNameDisplay.IngredientToResult;
                complexRecipe3.fabricators = new List<Tag>
                {
                    TagManager.Create("RockCrusher")
                };
                ComplexRecipeManager.Get().AddObsoleteIDMapping(obsolete_id3, text3);
                Element element4 = ElementLoader.FindElementByHash(SimHashes.Lime);
                ComplexRecipe.RecipeElement[] array7 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("BabyCrabShell", 1f)// 小蟹壳产石灰
                };
                ComplexRecipe.RecipeElement[] array8 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(element4.tag, 5f, ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false)
                };
                ComplexRecipe complexRecipe4 = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("RockCrusher", array7, array8), array7, array8);
                complexRecipe4.time = 30f;// 修改时间
                complexRecipe4.description = string.Format(STRINGS.BUILDINGS.PREFABS.ROCKCRUSHER.LIME_RECIPE_DESCRIPTION, SimHashes.Lime.CreateTag().ProperName(), STRINGS.ITEMS.INDUSTRIAL_PRODUCTS.CRAB_SHELL.NAME);
                complexRecipe4.nameDisplay = ComplexRecipe.RecipeNameDisplay.IngredientToResult;
                complexRecipe4.fabricators = new List<Tag>
                {
                    TagManager.Create("RockCrusher")
                };
                Element element5 = ElementLoader.FindElementByHash(SimHashes.Lime);
                ComplexRecipe.RecipeElement[] array9 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("CrabShell", 1f)// 蟹壳产石灰
                };
                ComplexRecipe.RecipeElement[] array10 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(element5.tag, 10f, ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false)
                };
                ComplexRecipe complexRecipe5 = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("RockCrusher", array9, array10), array9, array10);
                complexRecipe5.time = 30f;// 修改时间
                complexRecipe5.description = string.Format(STRINGS.BUILDINGS.PREFABS.ROCKCRUSHER.LIME_RECIPE_DESCRIPTION, SimHashes.Lime.CreateTag().ProperName(), STRINGS.ITEMS.INDUSTRIAL_PRODUCTS.CRAB_SHELL.NAME);
                complexRecipe5.nameDisplay = ComplexRecipe.RecipeNameDisplay.IngredientToResult;
                complexRecipe5.fabricators = new List<Tag>
                {
                    TagManager.Create("RockCrusher")
                };
                ComplexRecipe.RecipeElement[] array11 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("BabyCrabWoodShell", 1f)// 小木蟹壳产木材
                };
                ComplexRecipe.RecipeElement[] array12 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("WoodLog", 10f, ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false)
                };
                ComplexRecipe complexRecipe6 = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("RockCrusher", array11, array12), array11, array12);
                complexRecipe6.time = 30f;// 修改时间
                complexRecipe6.description = string.Format(STRINGS.BUILDINGS.PREFABS.ROCKCRUSHER.LIME_RECIPE_DESCRIPTION, WoodLogConfig.TAG.ProperName(), STRINGS.ITEMS.INDUSTRIAL_PRODUCTS.BABY_CRAB_SHELL.VARIANT_WOOD.NAME);
                complexRecipe6.nameDisplay = ComplexRecipe.RecipeNameDisplay.IngredientToResult;
                complexRecipe6.fabricators = new List<Tag>
                {
                    TagManager.Create("RockCrusher")
                };
                float num = 5f;
                ComplexRecipe.RecipeElement[] array13 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("CrabWoodShell", num)// 木蟹壳产木材
                };
                ComplexRecipe.RecipeElement[] array14 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("WoodLog", 100f * num, ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false)
                };
                ComplexRecipe complexRecipe7 = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("RockCrusher", array13, array14), array13, array14);
                complexRecipe7.time = 30f;// 修改时间
                complexRecipe7.description = string.Format(STRINGS.BUILDINGS.PREFABS.ROCKCRUSHER.LIME_RECIPE_DESCRIPTION, WoodLogConfig.TAG.ProperName(), STRINGS.ITEMS.INDUSTRIAL_PRODUCTS.CRAB_SHELL.VARIANT_WOOD.NAME);
                complexRecipe7.nameDisplay = ComplexRecipe.RecipeNameDisplay.IngredientToResult;
                complexRecipe7.fabricators = new List<Tag>
                {
                    TagManager.Create("RockCrusher")
                };
                ComplexRecipe.RecipeElement[] array15 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(ElementLoader.FindElementByHash(SimHashes.Fossil).tag, 50f)// 修改化石消耗
                };
                ComplexRecipe.RecipeElement[] array16 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(ElementLoader.FindElementByHash(SimHashes.Sand).tag, 50f, ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false),// 产出沙子50kg
                    new ComplexRecipe.RecipeElement(ElementLoader.FindElementByHash(SimHashes.Lime).tag, 100f, ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false)// 产出石灰100kg
                };
                ComplexRecipe complexRecipe8 = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("RockCrusher", array15, array16), array15, array16);
                complexRecipe8.time = 30f;// 修改时间
                complexRecipe8.description = string.Format(STRINGS.BUILDINGS.PREFABS.ROCKCRUSHER.LIME_FROM_LIMESTONE_RECIPE_DESCRIPTION, SimHashes.Fossil.CreateTag().ProperName(), SimHashes.SedimentaryRock.CreateTag().ProperName(), SimHashes.Lime.CreateTag().ProperName());
                complexRecipe8.nameDisplay = ComplexRecipe.RecipeNameDisplay.IngredientToResult;
                complexRecipe8.fabricators = new List<Tag>
                {
                    TagManager.Create("RockCrusher")
                };
                ComplexRecipe.RecipeElement[] array17 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("GarbageElectrobank", 1f)// 没启用配方
                };
                ComplexRecipe.RecipeElement[] array18 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(ElementLoader.FindElementByHash(SimHashes.Katairite).tag, 100f, ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false)
                };
                ComplexRecipe complexRecipe9 = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("RockCrusher", array17, array18), array17, array18, DlcManager.DLC3);
                complexRecipe9.time = 40f;
                complexRecipe9.description = string.Format(STRINGS.BUILDINGS.PREFABS.ROCKCRUSHER.RECIPE_DESCRIPTION, STRINGS.ITEMS.INDUSTRIAL_PRODUCTS.ELECTROBANK_GARBAGE.NAME, SimHashes.Katairite.CreateTag().ProperName());
                complexRecipe9.nameDisplay = ComplexRecipe.RecipeNameDisplay.Ingredient;
                complexRecipe9.fabricators = new List<Tag>
                {
                    TagManager.Create("RockCrusher")
                };
                float num2 = 0.05f;
                ComplexRecipe.RecipeElement[] array19 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(SimHashes.Salt.CreateTag(), 50f)// 修改盐矿消耗
                };
                ComplexRecipe.RecipeElement[] array20 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(TableSaltConfig.ID.ToTag(), 100f * num2, ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false),// 产出食盐5kg
                    new ComplexRecipe.RecipeElement(SimHashes.Sand.CreateTag(), 100f * (1f - num2), ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false)// 产出沙子95kg
                };
                ComplexRecipe complexRecipe10 = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("RockCrusher", array19, array20), array19, array20);
                complexRecipe10.time = 30f;// 修改时间
                complexRecipe10.description = string.Format(STRINGS.BUILDINGS.PREFABS.ROCKCRUSHER.RECIPE_DESCRIPTION, SimHashes.Salt.CreateTag().ProperName(), STRINGS.ITEMS.INDUSTRIAL_PRODUCTS.TABLE_SALT.NAME);
                complexRecipe10.nameDisplay = ComplexRecipe.RecipeNameDisplay.IngredientToResult;
                complexRecipe10.fabricators = new List<Tag>
                {
                    TagManager.Create("RockCrusher")
                };
                // 新增配方
                float new_num1 = 100f;
                ComplexRecipe.RecipeElement[] new_array1 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(SimHashes.SedimentaryRock.CreateTag(), 50f)// 修改沉积岩消耗
                };
                ComplexRecipe.RecipeElement[] new_array2 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(SimHashes.Lime.CreateTag(), new_num1, ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false)// 产出石灰100kg
                };
                ComplexRecipe complexRecipe14 = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("RockCrusher", new_array1, new_array2), new_array1, new_array2);
                complexRecipe14.time = 30f;// 修改时间
                complexRecipe14.description = string.Format(STRINGS.BUILDINGS.PREFABS.ROCKCRUSHER.RECIPE_DESCRIPTION, SimHashes.Salt.CreateTag().ProperName(), STRINGS.ITEMS.INDUSTRIAL_PRODUCTS.TABLE_SALT.NAME);
                complexRecipe14.nameDisplay = ComplexRecipe.RecipeNameDisplay.IngredientToResult;
                complexRecipe14.fabricators = new List<Tag>
                {
                    TagManager.Create("RockCrusher")
                };
                ComplexRecipe.RecipeElement[] new_array3 = new ComplexRecipe.RecipeElement[]
{
                    new ComplexRecipe.RecipeElement(SimHashes.SandStone.CreateTag(), 50f)// 修改砂岩消耗
};
                ComplexRecipe.RecipeElement[] new_array4 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(SimHashes.Clay.CreateTag(), new_num1, ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false)// 产出粘土100kg
                };
                ComplexRecipe complexRecipe15 = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("RockCrusher", new_array3, new_array4), new_array3, new_array4);
                complexRecipe15.time = 30f;// 修改时间
                complexRecipe15.description = string.Format(STRINGS.BUILDINGS.PREFABS.ROCKCRUSHER.RECIPE_DESCRIPTION, SimHashes.Salt.CreateTag().ProperName(), STRINGS.ITEMS.INDUSTRIAL_PRODUCTS.TABLE_SALT.NAME);
                complexRecipe15.nameDisplay = ComplexRecipe.RecipeNameDisplay.IngredientToResult;
                complexRecipe15.fabricators = new List<Tag>
                {
                    TagManager.Create("RockCrusher")
                };
                if (ElementLoader.FindElementByHash(SimHashes.Graphite) != null)
                {
                    float num3 = 0.9f;
                    ComplexRecipe.RecipeElement[] array21 = new ComplexRecipe.RecipeElement[]
                    {
                        new ComplexRecipe.RecipeElement(SimHashes.Fullerene.CreateTag(), 100f)// 富勒烯产石墨和沙子
                    };
                    ComplexRecipe.RecipeElement[] array22 = new ComplexRecipe.RecipeElement[]
                    {
                        new ComplexRecipe.RecipeElement(SimHashes.Graphite.CreateTag(), 100f * num3, ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false),
                        new ComplexRecipe.RecipeElement(SimHashes.Sand.CreateTag(), 100f * (1f - num3), ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false)
                    };
                    ComplexRecipe complexRecipe11 = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("RockCrusher", array21, array22), array21, array22, DlcManager.AVAILABLE_EXPANSION1_ONLY);
                    complexRecipe11.time = 30f;// 修改时间
                    complexRecipe11.description = string.Format(STRINGS.BUILDINGS.PREFABS.ROCKCRUSHER.RECIPE_DESCRIPTION, SimHashes.Fullerene.CreateTag().ProperName(), SimHashes.Graphite.CreateTag().ProperName());
                    complexRecipe11.nameDisplay = ComplexRecipe.RecipeNameDisplay.IngredientToResult;
                    complexRecipe11.fabricators = new List<Tag>
                    {
                        TagManager.Create("RockCrusher")
                    };
                }
                float num4 = 120f;
                float num5 = num4 * 0.5f;
                ComplexRecipe.RecipeElement[] array23 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("IceBellyPoop", num4)// 绒犸兔碎糜
                };
                ComplexRecipe.RecipeElement[] array24 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(SimHashes.Phosphorite.CreateTag(), num4 + num5, ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false),// 产出磷矿180kg
                    new ComplexRecipe.RecipeElement(SimHashes.Clay.CreateTag(), num4 + num5, ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false)// 产出粘土180kg
                };
                ComplexRecipe complexRecipe12 = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("RockCrusher", array23, array24), array23, array24, DlcManager.AVAILABLE_DLC_2);
                complexRecipe12.time = 40f;
                complexRecipe12.description = string.Format(STRINGS.BUILDINGS.PREFABS.ROCKCRUSHER.RECIPE_DESCRIPTION_TWO_OUTPUT, STRINGS.ITEMS.INDUSTRIAL_PRODUCTS.ICE_BELLY_POOP.NAME, SimHashes.Phosphorite.CreateTag().ProperName(), SimHashes.Clay.CreateTag().ProperName());
                complexRecipe12.nameDisplay = ComplexRecipe.RecipeNameDisplay.Ingredient;
                complexRecipe12.fabricators = new List<Tag>
                {
                    TagManager.Create("RockCrusher")
                };
                ComplexRecipe.RecipeElement[] array25 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("GoldBellyCrown", 1f)
                };
                ComplexRecipe.RecipeElement[] array26 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(ElementLoader.FindElementByHash(SimHashes.GoldAmalgam).tag, GoldBellyConfig.GOLD_PER_CYCLE * GoldBellyConfig.SCALE_GROWTH_TIME_IN_CYCLES, ComplexRecipe.RecipeElement.TemperatureOperation.AverageTemperature, false)
                };
                ComplexRecipe complexRecipe13 = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("RockCrusher", array25, array26), array25, array26, DlcManager.AVAILABLE_DLC_2);
                complexRecipe13.time = 40f;
                complexRecipe13.description = string.Format(STRINGS.BUILDINGS.PREFABS.ROCKCRUSHER.RECIPE_DESCRIPTION, STRINGS.ITEMS.INDUSTRIAL_PRODUCTS.GOLD_BELLY_CROWN.NAME, SimHashes.GoldAmalgam.CreateTag().ProperName());
                complexRecipe13.nameDisplay = ComplexRecipe.RecipeNameDisplay.Ingredient;
                complexRecipe13.fabricators = new List<Tag>
                {
                    TagManager.Create("RockCrusher")
                };
                Prioritizable.AddRef(go);
                return false;// 返回false以阻止原方法执行
            }
        }
    }
}
