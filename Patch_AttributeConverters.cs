﻿using System.Collections.Generic;
using System.Linq;
using Database;
using HarmonyLib;

namespace mymod
{
    [HarmonyPatch(typeof(AttributeConverters), MethodType.Constructor)]
    public class Patch_AttributeConverters
    {
        public static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)// 小人属性加成修改，调用IL指令，谨慎修改
        {
            List<CodeInstruction> codes = new List<CodeInstruction>(instructions);
            codes[19].operand = 0.1f;// MovementSpeed
            codes[34].operand = 0.5f;// ConstructionSpeed
            codes[49].operand = 0.5f;// DiggingSpeed
            codes[64].operand = 0.2f;// MachinerySpeed
            codes[79].operand = 0.1f;// HarvestSpeed
            codes[94].operand = 0.05f;// PlantTendSpeed
            codes[109].operand = 0.2f;// CompoundingSpeed
            codes[124].operand = 0.8f;// ResearchSpeed
            codes[139].operand = 0.2f;// TrainingSpeed
            codes[154].operand = 0.1f;// CookingSpeed
            codes[169].operand = 0.2f;// ArtSpeed
            codes[184].operand = 0.4f;// DoctorSpeed
            codes[199].operand = 0.5f;// TidyingSpeed
            codes[214].operand = 0.1f;// AttackDamage
            codes[229].operand = 0.05f;// PilotingSpeed
            codes[244].operand = 0.004f;// ImmuneLevelBoost
            codes[275].operand = 100f;// CarryAmountFromStrength
            codes[290].operand = 0.2f;// TemperatureInsulation
            codes[305].operand = 0.06f;// SeedHarvestChance
            codes[320].operand = 0.1f;// CapturableSpeed
            codes[335].operand = 0.1f;// GeotuningSpeed
            codes[350].operand = 0.2f;// RanchingEffectDuration
            codes[365].operand = 0.2f;// FarmedEffectDuration
            codes[380].operand = 0.05f;// PowerTinkerEffectDuration

            return instructions.AsEnumerable<CodeInstruction>();
            /*
            foreach (CodeInstruction code in codes)
            {
                bool flag = code.opcode == OpCodes.Ldc_R4;
                if (flag)
                {
                    object operand = code.operand;
                    bool flag2;
                    if (operand is float)
                    {
                        float value = (float)operand;
                        flag2 = (value == 0.1f);
                    }
                    else
                    {
                        flag2 = false;
                    }
                    bool flag3 = flag2;
                    if (flag3)
                    {
                        code.operand = 0.15f;
                    }
                    operand = code.operand;
                    bool flag4;
                    if (operand is float)
                    {
                        float value2 = (float)operand;
                        flag4 = (value2 == 0.25f);
                    }
                    else
                    {
                        flag4 = false;
                    }
                    bool flag5 = flag4;
                    if (flag5)
                    {
                        code.operand = 0.5f;
                    }
                    operand = code.operand;
                    bool flag6;
                    if (operand is float)
                    {
                        float value3 = (float)operand;
                        flag6 = (value3 == 0.05f);
                    }
                    else
                    {
                        flag6 = false;
                    }
                    bool flag7 = flag6;
                    if (flag7)
                    {
                        code.operand = 0.1f;
                    }
                    operand = code.operand;
                    bool flag8;
                    if (operand is float)
                    {
                        float value4 = (float)operand;
                        flag8 = (value4 == 0.025f);
                    }
                    else
                    {
                        flag8 = false;
                    }
                    bool flag9 = flag8;
                    if (flag9)
                    {
                        code.operand = 0.05f;
                    }
                    operand = code.operand;
                    bool flag10;
                    if (operand is float)
                    {
                        float value5 = (float)operand;
                        flag10 = (value5 == 0.4f);
                    }
                    else
                    {
                        flag10 = false;
                    }
                    bool flag11 = flag10;
                    if (flag11)
                    {
                        code.operand = 0.8f;
                    }
                    operand = code.operand;
                    bool flag12;
                    if (operand is float)
                    {
                        float value6 = (float)operand;
                        flag12 = (value6 == 0.2f);
                    }
                    else
                    {
                        flag12 = false;
                    }
                    bool flag13 = flag12;
                    if (flag13)
                    {
                        code.operand = 0.4f;
                    }
                    operand = code.operand;
                    bool flag14;
                    if (operand is float)
                    {
                        float value7 = (float)operand;
                        flag14 = (value7 == 0.033f);
                    }
                    else
                    {
                        flag14 = false;
                    }
                    bool flag15 = flag14;
                    if (flag15)
                    {
                        code.operand = 0.08f;
                    }
                    operand = code.operand;
                    bool flag16;
                    if (operand is float)
                    {
                        float value8 = (float)operand;
                        flag16 = (value8 == 40f);
                    }
                    else
                    {
                        flag16 = false;
                    }
                    bool flag17 = flag16;
                    if (flag17)
                    {
                        code.operand = 100f;
                    }
                    operand = code.operand;
                    bool flag18;
                    if (operand is float)
                    {
                        float value9 = (float)operand;
                        flag18 = (value9 == 0.0016666667f);
                    }
                    else
                    {
                        flag18 = false;
                    }
                    bool flag19 = flag18;
                    if (flag19)
                    {
                        code.operand = 0.004f;
                    }
                }
            }
            return instructions.AsEnumerable<CodeInstruction>();
            */
        }
    }
}
