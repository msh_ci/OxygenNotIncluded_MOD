﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using HarmonyLib;
using UnityEngine;

namespace mymod
{
    [HarmonyPatch(typeof(MinionResume))]// 修改升级速度
    [HarmonyPatch("AddExperience")]
    public class Patch_MinionResume
    {
        public static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instr)
        {
            List<CodeInstruction> code = instr.ToList();
            // 创建新的指令
            CodeInstruction ldcR4Instruction = new CodeInstruction(OpCodes.Ldc_R4, 2f);
            CodeInstruction mulInstruction = new CodeInstruction(OpCodes.Mul);
            // 在第10行之后插入新指令
            if (code.Count > 10)
            {
                code.Insert(11, mulInstruction); // 插入乘法指令
                code.Insert(10, ldcR4Instruction); // 插入加载浮点数2.0的指令
            }
            return code.AsEnumerable();
        }
    }
    [HarmonyPatch(typeof(PinkRockCarvedConfig))]// 采掘的流明石英
    [HarmonyPatch("CreatePrefab")]
    public static class Patch_PinkRock
    {
        public static void Postfix(GameObject __result)
        {
            if (__result != null)
            {
                if (DlcManager.FeatureRadiationEnabled())
                {
                    RadiationEmitter radiationEmitter = __result.AddComponent<RadiationEmitter>();
                    radiationEmitter.emitType = RadiationEmitter.RadiationEmitterType.Constant;
                    radiationEmitter.radiusProportionalToRads = false;
                    radiationEmitter.emitRadiusX = 5;
                    radiationEmitter.emitRadiusY = radiationEmitter.emitRadiusX;
                    radiationEmitter.emitRads = 480f;
                    radiationEmitter.emissionOffset = new Vector3(0f, 0f, 0f);
                }
            }
        }
    }
    [HarmonyPatch(typeof(SolidConduitDispenser))]// 修改运输轨道容量
    [HarmonyPatch("ConduitUpdate")]
    public class Patch_SolidConduitDispenser
    {
        public static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instr)
        {
            List<CodeInstruction> code = instr.ToList();
            code[37].operand = 100f;// 100kg每格
            code[40].operand = 100f;
            return code.AsEnumerable();
        }
    }
    [HarmonyPatch(typeof(MetalRefineryConfig))]// 金属精炼器，配方更新易报错
    [HarmonyPatch("ConfigureBuildingTemplate")]
    public class Patch_MetalRefinery
    {
        public static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instr)
        {
            List<CodeInstruction> code = instr.ToList();
            code[84].operand = 1200f;// 容量1200kg，每次消耗400kg，让炼钢可以循环
            code[174].operand = 30f;// 30秒
            code[264].operand = 30f;
            return code.AsEnumerable();
        }
    }
    [HarmonyPatch(typeof(SupermaterialRefineryConfig))]// 分子熔炉，配方更新易报错
    [HarmonyPatch("ConfigureBuildingTemplate")]
    public class Patch_SupermaterialRefinery
    {
        public static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instr)
        {
            List<CodeInstruction> code = instr.ToList();
            code[91].operand = 50f;// 50秒
            code[168].operand = 50f;
            code[239].operand = 50f;
            code[314].operand = 50f;
            code[374].operand = 50f;
            code[434].operand = 50f;
            code[497].operand = 50f;
            return code.AsEnumerable();
        }
    }
}