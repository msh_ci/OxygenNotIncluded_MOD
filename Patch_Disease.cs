﻿using System.Collections.Generic;
using System.Linq;
using HarmonyLib;
using Klei.AI;

namespace mymod// 病菌修改，调用IL指令，谨慎修改
{
    [HarmonyPatch(typeof(SlimeGerms), MethodType.Constructor, typeof(bool))]
    public class Patch_SlimeGerms
    {
        public static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        {
            List<CodeInstruction> codes = new List<CodeInstruction>(instructions);
            // Debug.Log($"codes:{codes.Count}");
            codes[3].operand = 103.15f;
            codes[4].operand = 113.15f;
            codes[5].operand = 133.15f;
            codes[6].operand = 143.15f;

            return instructions.AsEnumerable<CodeInstruction>();
        }
    }

    [HarmonyPatch(typeof(FoodGerms), MethodType.Constructor, typeof(bool))]
    public class Patch_FoodGerms
    {
        public static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        {
            List<CodeInstruction> codes = new List<CodeInstruction>(instructions);
            // Debug.Log($"codes:{codes.Count}");
            codes[3].operand = 103.15f;
            codes[4].operand = 113.15f;
            codes[5].operand = 133.15f;
            codes[6].operand = 143.15f;

            return instructions.AsEnumerable<CodeInstruction>();
        }
    }

    [HarmonyPatch(typeof(ZombieSpores), MethodType.Constructor, typeof(bool))]
    public class Patch_ZombieSpores
    {
        public static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        {
            List<CodeInstruction> codes = new List<CodeInstruction>(instructions);
            // Debug.Log($"codes:{codes.Count}");
            codes[3].operand = 103.15f;
            codes[4].operand = 113.15f;
            codes[5].operand = 133.15f;
            codes[6].operand = 143.15f;

            return instructions.AsEnumerable<CodeInstruction>();
        }
    }
}
