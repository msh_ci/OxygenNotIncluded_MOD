﻿using System;
using HarmonyLib;

namespace mymod
{
    /*
    public class Patch_RoomTypes
    {
        [HarmonyPatch(typeof(RoomProber), "RefreshRooms")]
        public class RoomProber_RefreshRooms_Reset
        {
            public static void Postfix()
            {
                TuningData<RoomProber.Tuning>.Get().maxRoomSize = 160;// 修改默认最大房间大小
            }
        }
        [HarmonyPatch(typeof(RoomType))]
        [HarmonyPatch(MethodType.Constructor)]
        [HarmonyPatch(new Type[]
        {
            typeof(string),
            typeof(string),
            typeof(string),
            typeof(string),
            typeof(string),
            typeof(RoomTypeCategory),
            typeof(RoomConstraints.Constraint),
            typeof(RoomConstraints.Constraint[]),
            typeof(RoomDetails.Detail[]),
            typeof(int),
            typeof(RoomType[]),
            typeof(bool),
            typeof(bool),
            typeof(string[]),
            typeof(int)
        })]
        public static class Patch_roomssize
        {
            public static void Postfix(RoomType __instance)
            {
                bool f_Private1 = __instance.Id == "Private Bedroom";
                bool f_Private2 = f_Private1;
                if (f_Private2)
                {
                    for (int i = 0; i < __instance.additional_constraints.Length; i++)
                    {
                        bool f_Private3 = __instance.additional_constraints[i] == RoomConstraints.MINIMUM_SIZE_24;
                        bool f_Private4 = f_Private3;
                        if (f_Private4)
                        {
                            __instance.additional_constraints[i] = RoomConstraints.MINIMUM_SIZE_12;// 修改私人卧室最小尺寸为12
                        }
                    }
                }
                bool f_Hospital1 = __instance.Id == "Hospital";
                bool f_Hospital2 = f_Hospital1;
                if (f_Hospital2)
                {
                    for (int i = 0; i < __instance.additional_constraints.Length; i++)
                    {
                        bool f_Hospital3 = __instance.additional_constraints[i] == RoomConstraints.MAXIMUM_SIZE_96;
                        bool f_Hospital4 = f_Hospital3;
                        if (f_Hospital4)
                        {
                            __instance.additional_constraints[i] = RoomConstraints.MAXIMUM_SIZE_120;// 修改医院最小尺寸为120
                        }
                    }
                }
            }
        }
    }
    */
}
