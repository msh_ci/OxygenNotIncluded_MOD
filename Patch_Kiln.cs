﻿using System.Collections.Generic;
using HarmonyLib;

namespace mymod
{
    public static class Patch_Kiln// 建筑固定代码
    {
        [HarmonyPatch(typeof(KilnConfig))]// 窑炉
        [HarmonyPatch("ConfigureRecipes")]
        public static class Patch_KilnConfig
        {
            [HarmonyPrefix]
            public static bool Prefix(KilnConfig __instance)
            {
                // 完全替换原有方法的逻辑
                CustomConfigureRecipes(__instance);
                return false; // 阻止原始方法执行
            }
            public static void CustomConfigureRecipes(KilnConfig __instance)
            {
                Tag tag = SimHashes.Ceramic.CreateTag();// 陶瓷
                Tag material = SimHashes.Clay.CreateTag();// 粘土
                Tag material2 = SimHashes.Carbon.CreateTag();// 煤炭
                float num = 100f;
                float num2 = 25f;
                ComplexRecipe.RecipeElement[] array = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(material, num2),
                    new ComplexRecipe.RecipeElement(material2, num2)
                };
                ComplexRecipe.RecipeElement[] array2 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(tag, num + num2, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)// 修改陶瓷产量
                };
                string obsolete_id = ComplexRecipeManager.MakeObsoleteRecipeID("Kiln", tag);
                string text = ComplexRecipeManager.MakeRecipeID("Kiln", array, array2);
                ComplexRecipe complexRecipe = new ComplexRecipe(text, array, array2);
                complexRecipe.time = 30f;// 修改时间
                complexRecipe.description = string.Format(STRINGS.BUILDINGS.PREFABS.EGGCRACKER.RECIPE_DESCRIPTION, ElementLoader.FindElementByHash(SimHashes.Clay).name, ElementLoader.FindElementByHash(SimHashes.Ceramic).name);
                complexRecipe.fabricators = new List<Tag>
                {
                    TagManager.Create("Kiln")
                };
                complexRecipe.nameDisplay = ComplexRecipe.RecipeNameDisplay.Result;
                complexRecipe.sortOrder = 100;
                ComplexRecipeManager.Get().AddObsoleteIDMapping(obsolete_id, text);
                Tag tag2 = SimHashes.RefinedCarbon.CreateTag();
                ComplexRecipe.RecipeElement[] array3 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(material2, num - num2)// 修改煤炭消耗量
                };
                ComplexRecipe.RecipeElement[] array4 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(tag2, num2 + num, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)// 修改精炼碳产量
                };
                string obsolete_id2 = ComplexRecipeManager.MakeObsoleteRecipeID("Kiln", tag2);
                string text2 = ComplexRecipeManager.MakeRecipeID("Kiln", array3, array4);
                ComplexRecipe complexRecipe2 = new ComplexRecipe(text2, array3, array4);
                complexRecipe2.time = 30f;// 修改时间
                complexRecipe2.description = string.Format(STRINGS.BUILDINGS.PREFABS.EGGCRACKER.RECIPE_DESCRIPTION, ElementLoader.FindElementByHash(SimHashes.Carbon).name, ElementLoader.FindElementByHash(SimHashes.RefinedCarbon).name);
                complexRecipe2.fabricators = new List<Tag>
                {
                    TagManager.Create("Kiln")
                };
                complexRecipe2.nameDisplay = ComplexRecipe.RecipeNameDisplay.IngredientToResult;
                complexRecipe2.sortOrder = 200;
                ComplexRecipeManager.Get().AddObsoleteIDMapping(obsolete_id2, text2);
                Tag tag3 = SimHashes.RefinedCarbon.CreateTag();
                ComplexRecipe.RecipeElement[] array5 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(SimHashes.WoodLog.CreateTag(), 100f)
                };
                ComplexRecipe.RecipeElement[] array6 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(tag3, 200f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)// 修改精炼碳产量
                };
                string obsolete_id3 = ComplexRecipeManager.MakeObsoleteRecipeID("Kiln", tag3);
                string text3 = ComplexRecipeManager.MakeRecipeID("Kiln", array5, array6);
                ComplexRecipe complexRecipe3 = new ComplexRecipe(text3, array5, array6);
                complexRecipe3.time = 30f;
                complexRecipe3.description = string.Format(STRINGS.BUILDINGS.PREFABS.EGGCRACKER.RECIPE_DESCRIPTION, ElementLoader.FindElementByHash(SimHashes.WoodLog).name, ElementLoader.FindElementByHash(SimHashes.RefinedCarbon).name);
                complexRecipe3.fabricators = new List<Tag>
                {
                    TagManager.Create("Kiln")
                };
                complexRecipe3.nameDisplay = ComplexRecipe.RecipeNameDisplay.IngredientToResult;
                complexRecipe3.sortOrder = 300;
                ComplexRecipeManager.Get().AddObsoleteIDMapping(obsolete_id3, text3);
            }
        }
    }
}
