﻿using System.Collections.Generic;
using HarmonyLib;
using TUNING;

namespace mymod
{
    public static class Patch_CookingStationConfig// 建筑固定代码
    {
        [HarmonyPatch(typeof(CookingStationConfig))]// 电动烤炉
        [HarmonyPatch("ConfigureRecipes")]
        public static class Patch_CookingStation
        {
            [HarmonyPrefix]
            public static bool Prefix(CookingStationConfig __instance)
            {
                // 完全替换原有方法的逻辑
                CustomConfigureRecipes(__instance);
                return false; // 阻止原始方法执行
            }
            public static void CustomConfigureRecipes(CookingStationConfig __instance)
            {
                ComplexRecipe.RecipeElement[] array = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("BasicPlantFood", 3f)
                };
                ComplexRecipe.RecipeElement[] array2 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("PickledMeal", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                PickledMealConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("CookingStation", array, array2), array, array2)
                {
                    time = FOOD.RECIPES.SMALL_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.PICKLEDMEAL.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "CookingStation"
                    },
                    sortOrder = 21
                };
                ComplexRecipe.RecipeElement[] array3 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("MushBar", 1f)
                };
                ComplexRecipe.RecipeElement[] array4 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("FriedMushBar".ToTag(), 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                FriedMushBarConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("CookingStation", array3, array4), array3, array4)
                {
                    time = FOOD.RECIPES.STANDARD_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.FRIEDMUSHBAR.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "CookingStation"
                    },
                    sortOrder = 1
                };
                ComplexRecipe.RecipeElement[] array5 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(MushroomConfig.ID, 1f)
                };
                ComplexRecipe.RecipeElement[] array6 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("FriedMushroom", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                FriedMushroomConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("CookingStation", array5, array6), array5, array6)
                {
                    time = FOOD.RECIPES.STANDARD_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.FRIEDMUSHROOM.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "CookingStation"
                    },
                    sortOrder = 20
                };
                ComplexRecipe.RecipeElement[] array7 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("RawEgg", 1f),
                    new ComplexRecipe.RecipeElement("ColdWheatSeed", 2f)
                };
                ComplexRecipe.RecipeElement[] array8 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("Pancakes", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                CookedEggConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("CookingStation", array7, array8), array7, array8)
                {
                    time = FOOD.RECIPES.STANDARD_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.PANCAKES.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "CookingStation"
                    },
                    sortOrder = 20
                };
                ComplexRecipe.RecipeElement[] array9 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("Meat", 1f)
                };
                ComplexRecipe.RecipeElement[] array10 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("CookedMeat", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                CookedMeatConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("CookingStation", array9, array10), array9, array10)
                {
                    time = FOOD.RECIPES.STANDARD_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.COOKEDMEAT.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "CookingStation"
                    },
                    sortOrder = 21
                };
                ComplexRecipe.RecipeElement[] array11 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("FishMeat", 1f)
                };
                ComplexRecipe.RecipeElement[] array12 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("CookedFish", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                CookedMeatConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("CookingStation", array11, array12), array11, array12)
                {
                    time = 30f,
                    description = STRINGS.ITEMS.FOOD.COOKEDMEAT.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.IngredientToResult,
                    fabricators = new List<Tag>
                    {
                        "CookingStation"
                    },
                    sortOrder = 22
                };
                ComplexRecipe.RecipeElement[] array13 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("ShellfishMeat", 1f)
                };
                ComplexRecipe.RecipeElement[] array14 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("CookedFish", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                CookedMeatConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("CookingStation", array13, array14), array13, array14)
                {
                    time = FOOD.RECIPES.STANDARD_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.COOKEDMEAT.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.IngredientToResult,
                    fabricators = new List<Tag>
                    {
                        "CookingStation"
                    },
                    sortOrder = 22
                };
                ComplexRecipe.RecipeElement[] array15 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement(PrickleFruitConfig.ID, 1f)
                };
                ComplexRecipe.RecipeElement[] array16 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("GrilledPrickleFruit", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                GrilledPrickleFruitConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("CookingStation", array15, array16), array15, array16)
                {
                    time = FOOD.RECIPES.STANDARD_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.GRILLEDPRICKLEFRUIT.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "CookingStation"
                    },
                    sortOrder = 20
                };
                if (DlcManager.IsExpansion1Active())
                {
                    ComplexRecipe.RecipeElement[] array17 = new ComplexRecipe.RecipeElement[]
                    {
                        new ComplexRecipe.RecipeElement(SwampFruitConfig.ID, 1f)
                    };
                    ComplexRecipe.RecipeElement[] array18 = new ComplexRecipe.RecipeElement[]
                    {
                        new ComplexRecipe.RecipeElement("SwampDelights", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                    };
                    CookedEggConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("CookingStation", array17, array18), array17, array18)
                    {
                        time = FOOD.RECIPES.STANDARD_COOK_TIME,
                        description = STRINGS.ITEMS.FOOD.SWAMPDELIGHTS.RECIPEDESC,
                        nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                        fabricators = new List<Tag>
                        {
                            "CookingStation"
                        },
                        sortOrder = 20
                    };
                }
                ComplexRecipe.RecipeElement[] array19 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("ColdWheatSeed", 2f)
                };
                ComplexRecipe.RecipeElement[] array20 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("ColdWheatBread", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                ColdWheatBreadConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("CookingStation", array19, array20), array19, array20)
                {
                    time = 30f,
                    description = STRINGS.ITEMS.FOOD.COLDWHEATBREAD.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "CookingStation"
                    },
                    sortOrder = 50
                };
                ComplexRecipe.RecipeElement[] array21 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("RawEgg", 1f)
                };
                ComplexRecipe.RecipeElement[] array22 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("CookedEgg", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                CookedEggConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("CookingStation", array21, array22), array21, array22)
                {
                    time = FOOD.RECIPES.STANDARD_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.COOKEDEGG.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "CookingStation"
                    },
                    sortOrder = 1
                };
                if (DlcManager.IsExpansion1Active())
                {
                    ComplexRecipe.RecipeElement[] array23 = new ComplexRecipe.RecipeElement[]
                    {
                        new ComplexRecipe.RecipeElement("WormBasicFruit", 1f)
                    };
                    ComplexRecipe.RecipeElement[] array24 = new ComplexRecipe.RecipeElement[]
                    {
                        new ComplexRecipe.RecipeElement("WormBasicFood", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                    };
                    WormBasicFoodConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("CookingStation", array23, array24), array23, array24)
                    {
                        time = FOOD.RECIPES.STANDARD_COOK_TIME,
                        description = STRINGS.ITEMS.FOOD.WORMBASICFOOD.RECIPEDESC,
                        nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                        fabricators = new List<Tag>
                        {
                            "CookingStation"
                        },
                        sortOrder = 20
                    };
                }
                if (DlcManager.IsExpansion1Active())
                {
                    ComplexRecipe.RecipeElement[] array25 = new ComplexRecipe.RecipeElement[]
                    {
                        new ComplexRecipe.RecipeElement("WormSuperFruit", 8f),
                        new ComplexRecipe.RecipeElement("Sucrose".ToTag(), 4f)
                    };
                    ComplexRecipe.RecipeElement[] array26 = new ComplexRecipe.RecipeElement[]
                    {
                        new ComplexRecipe.RecipeElement("WormSuperFood", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                    };
                    WormSuperFoodConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("CookingStation", array25, array26), array25, array26)
                    {
                        time = FOOD.RECIPES.STANDARD_COOK_TIME,
                        description = STRINGS.ITEMS.FOOD.WORMSUPERFOOD.RECIPEDESC,
                        nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                        fabricators = new List<Tag>
                        {
                            "CookingStation"
                        },
                        sortOrder = 20
                    };
                }
                ComplexRecipe.RecipeElement[] array27 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("HardSkinBerry", 1f)
                };
                ComplexRecipe.RecipeElement[] array28 = new ComplexRecipe.RecipeElement[]
                {
                    new ComplexRecipe.RecipeElement("CookedPikeapple", 1f, ComplexRecipe.RecipeElement.TemperatureOperation.Heated, false)
                };
                CookedPikeappleConfig.recipe = new ComplexRecipe(ComplexRecipeManager.MakeRecipeID("CookingStation", array27, array28), array27, array28, DlcManager.AVAILABLE_DLC_2)
                {
                    time = FOOD.RECIPES.STANDARD_COOK_TIME,
                    description = STRINGS.ITEMS.FOOD.COOKEDPIKEAPPLE.RECIPEDESC,
                    nameDisplay = ComplexRecipe.RecipeNameDisplay.Result,
                    fabricators = new List<Tag>
                    {
                        "CookingStation"
                    },
                    sortOrder = 18
                };
            }
        }
    }
}
