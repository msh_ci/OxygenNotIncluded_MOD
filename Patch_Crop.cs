﻿using System.Collections.Generic;
using HarmonyLib;
using TUNING;
using UnityEngine;

namespace mymod
{
    internal class Patch_Crop
    {
        [HarmonyPatch(typeof(Db))]
        [HarmonyPatch("Initialize")]
        public class Patch_CROPS
        {
            public static void Prefix()
            {
                CROPS.CROP_TYPES = new List<Crop.CropVal>
                {
                    new Crop.CropVal("BasicPlantFood", 1800f, 1, true),
                    new Crop.CropVal(PrickleFruitConfig.ID, 1800f, 2, true),
                    new Crop.CropVal(SwampFruitConfig.ID, 3960f, 1, true),
                    new Crop.CropVal(MushroomConfig.ID, 2400f, 2, true),
                    new Crop.CropVal("ColdWheatSeed", 5400f, 36, true),
                    new Crop.CropVal(SpiceNutConfig.ID, 4800f, 6, true),
                    new Crop.CropVal(BasicFabricConfig.ID, 1200f, 2, true),
                    new Crop.CropVal(SwampLilyFlowerConfig.ID, 7200f, 2, true),
                    new Crop.CropVal("GasGrassHarvested", 1200f, 1, true),
                    new Crop.CropVal("WoodLog", 2700f, 300, true),
                    new Crop.CropVal(SimHashes.WoodLog.ToString(), 1800f, 600, true),
                    new Crop.CropVal(SimHashes.SugarWater.ToString(), 150f, 20, true),
                    new Crop.CropVal("SpaceTreeBranch", 2700f, 1, true),
                    new Crop.CropVal("HardSkinBerry", 1800f, 1, true),
                    new Crop.CropVal(CarrotConfig.ID, 5400f, 1, true),
                    new Crop.CropVal(SimHashes.OxyRock.ToString(), 1200f, 2 * Mathf.RoundToInt(17.76f), true),
                    new Crop.CropVal("Lettuce", 3600f, 14, true),
                    new Crop.CropVal("BeanPlantSeed", 6000f, 30, true),
                    new Crop.CropVal("OxyfernSeed", 7200f, 1, true),
                    new Crop.CropVal("PlantMeat", 18000f, 10, true),
                    new Crop.CropVal("WormBasicFruit", 2400f, 1, true),
                    new Crop.CropVal("WormSuperFruit", 4800f, 8, true),
                    new Crop.CropVal(SimHashes.Salt.ToString(), 3600f, 65, true),
                    new Crop.CropVal(SimHashes.Water.ToString(), 6000f, 350, true)
                };
            }
        }
    }
}